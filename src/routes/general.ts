import express, { Request, Response } from 'express';
import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { SecurePass } from "argon2-pass";

import db from "../db/connection";

const router = express.Router();

router.get("/categories", async (req: Request, res: Response) => {
	let response = await db.query<RowDataPacket[]>(res, "SELECT id, nev AS name, ikon AS icon FROM kategoria");
	if (!response) return;

	let categories = response.map(i => ({
		id: Number(i.id),
		name: i.name,
		icon: (i.icon as Buffer).toString("base64")
	}));

	return res.send({
		status: "ok",
		categories
	});
});

export default router;