import express, { Request, Response } from 'express';
import { ResultSetHeader, RowDataPacket } from 'mysql2';

import db from "../db/connection";
import { required, one_required } from "../middlewares/required";
import auth, { AuthType } from '../middlewares/login';
import { v4 as uuidv4 } from "uuid";
import { user_owns_account } from '../utils/query';

const router = express.Router();

router.post("/add",
	auth(AuthType.Login),
	required("amount", "receiver_account", "sender_account"),
	async (req: Request, res: Response) => {
		let {
			amount, receiver_account, sender_account,
			message,
		} = req.body;
		amount = Number(amount);

		if (amount <= 0 || sender_account === receiver_account)
			return res.send({
				status: "error",
				msg: "Nice try :D"
			});

		// Get current users account number and balance
		let result = await db.query<RowDataPacket[]>(res, "SELECT szam AS number, egyenleg AS balance FROM folyoszamla WHERE ugyfel_id=?", [req.session.userid]);
		if (result === null) return;
		if (result.length === 0)
			return res.send({
				status: "error",
				msg: "A megadott ügyfél azonosító nem létezik a folyószámlák között!"
			});

		let sender_balance = Number(result.find(x => x.number == sender_account)?.balance);
		if (Number.isNaN(sender_balance))
			return res.send({
				status: "error",
				msg: "A megadott küldő számla nem létezik vagy nem az ön tulajdonában van!"
			});

		// Insert transaction into the database (even if it failed)
		let success = sender_balance >= amount;
		let transaction_id = uuidv4();
		let date = new Date().toISOString();

		let insert_res = await db.query<ResultSetHeader>(res,
			`INSERT INTO tranzakcio (id, osszeg, kozlemeny, datum, sikeres, kuldo_szamla, fogado_szamla, kategoria_id) VALUES (?, ?, ?, ?, ?, ?, ?, IFNULL((SELECT kategoria_id FROM ceges_folyoszamla WHERE szam=?), 11))`,
			[transaction_id, amount, message ?? null, date, success, sender_account, receiver_account, receiver_account],
			{ "ER_NO_REFERENCED_ROW_2": "A megadott számlaszám nem létezik" });
		if (!insert_res) return;

		if (!success)
			return res.send({
				status: "error",
				msg: "A tranzakció sikertelen! Indok: Nincs elég pénzed."
			});

		// Get reveiver's balance
		result = await db.query<RowDataPacket[]>(res,
			"SELECT egyenleg FROM folyoszamla WHERE szam=?", [receiver_account]);
		if (result === null) return;
		let receiver_balance = Number(result[0].egyenleg);

		// Update balance of both parties
		await db.query<ResultSetHeader>(res, "UPDATE folyoszamla SET egyenleg=? WHERE szam=?", [sender_balance - amount, sender_account]);
		await db.query<ResultSetHeader>(res, "UPDATE folyoszamla SET egyenleg=? WHERE szam=?", [receiver_balance + amount, receiver_account]);

		return res.send({
			status: "ok"
		});
	});

router.post("/changecategory",
	auth(AuthType.Login),
	required("transaction_id", "category"),
	async (req: Request, res: Response) => {
		let { transaction_id, category } = req.body;

		let validation_result = await db.query<RowDataPacket[]>(res,
			`SELECT ugyfel_id AS userid FROM 
			tranzakcio	
			INNER JOIN folyoszamla ON tranzakcio.fogado_szamla AND tranzakcio.kuldo_szamla
			WHERE folyoszamla.ugyfel_id = ? AND tranzakcio.id = ?;`, [req.session.userid, transaction_id]);
		if (!validation_result) return;
		if (!validation_result[0] || validation_result[0].userid !== req.session.userid)
			return res.send({
				status: "error",
				msg: "A megadott tranzakció nem létezik vagy nem az ön tulajdonában van!"
			});

		let category_result = await db.query<RowDataPacket[]>(res,
			`SELECT id FROM kategoria WHERE id=?`, [category]);
		if (!category_result) return;
		if (!category_result[0])
			return res.send({
				status: "error",
				msg: "A megadott kategória nem létezik!"
			});

		let result = await db.query<RowDataPacket[]>(res,
			`UPDATE tranzakcio SET kategoria_id=? WHERE id=?`, [category, transaction_id]);
		if (!result) return;

		return res.send({
			status: "ok"
		});
	});

router.post("/get",
	auth(AuthType.Login),
	required("account_number", "start_date", "end_date"),
	async (req: Request, res: Response) => {
		let {
			account_number,
			start_date, end_date
		} = req.body;

		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		let transactions = await db.query<RowDataPacket[]>(res,
			`
			SELECT
				tranzakcio.id AS id,
				tranzakcio.sikeres AS successful,
				folyoszamla.szam AS partner_account_number,
				folyoszamla.nev AS partner_account_name,
				tranzakcio.datum AS date,
				tranzakcio.kozlemeny AS message,
               	IFNULL(kategoria.id, 11) as category,
				-- Negatív lesz, ha a a lekérdező küldi a tranzakciót és pozitív ha fogadja
				CASE
					WHEN tranzakcio.kuldo_szamla = ? THEN -tranzakcio.osszeg
					ELSE tranzakcio.osszeg
				END AS amount
			FROM tranzakcio 
				INNER JOIN folyoszamla ON 
					-- Ha a lekérdező küldi a tranzakciót akkor a fogadó nevére vagyunk kíváncsiak és fordítva
					(CASE
						WHEN tranzakcio.kuldo_szamla = ? THEN tranzakcio.fogado_szamla
						ELSE tranzakcio.kuldo_szamla
					END) = folyoszamla.szam
				LEFT JOIN kategoria ON tranzakcio.kategoria_id = kategoria.id
			WHERE 
            	tranzakcio.datum BETWEEN ? AND ? AND 
                (tranzakcio.fogado_szamla = ? OR tranzakcio.kuldo_szamla = ?)
			ORDER BY tranzakcio.datum DESC;
			`, [account_number, account_number, start_date, end_date, account_number, account_number]);
		if (!transactions) return;

		return res.send({
			status: "ok",
			transactions
		});
	});

router.post("/recents",
	auth(AuthType.Login),
	required("account_number"),
	async (req: Request, res: Response) => {
		let {
			account_number, amount
		} = req.body;

		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		let results = await db.query<RowDataPacket[]>(res,
			`SELECT folyoszamla.szam AS number, folyoszamla.nev AS name, MAX(tranzakcio.datum) AS maxdate
			FROM
			tranzakcio
				INNER JOIN folyoszamla ON tranzakcio.fogado_szamla = folyoszamla.szam
			WHERE tranzakcio.kuldo_szamla = ?
			GROUP BY folyoszamla.szam
			ORDER BY maxdate DESC
			LIMIT ?;`, [account_number, amount ?? 5]);
		if (!results) return;

		results = results.map(r => ({
			number: r.number,
			name: r.name
		}) as RowDataPacket);

		return res.send({
			status: "ok",
			recents: results
		});
	});

export default router;