import express, { Request, Response, Router } from 'express';
import { ResultSetHeader, RowDataPacket } from 'mysql2';

import db from "../db/connection";
import { required, one_required } from "../middlewares/required";
import auth, { AuthType } from '../middlewares/login';
import { random_digits } from "../utils/random";
import { get_current_date } from '../utils/date';
import { user_owns_account } from "../utils/query";
import { v4 as uuidv4 } from "uuid";

const router = Router();

router.post("/create",
	auth(AuthType.Login),
	required("name"),
	async (req: Request, res: Response) => {
		let {
			name, category
		} = req.body;

		// Generate account number for the user
		let sender_account = `11773353${random_digits(8)}`;
		let result = await db.query<ResultSetHeader>(res,
			"INSERT INTO folyoszamla (szam, nev, egyenleg, nyitasi_datum, ugyfel_id) VALUES (?, ?, ?, ?, ?)",
			[sender_account, name, 0, get_current_date(), req.session.userid],
		);
		if (result === null) return;

		// If category is set it is a company account
		if (category) {
			result = await db.query<ResultSetHeader>(res,
				"INSERT INTO ceges_folyoszamla (szam, kategoria_id) VALUES (?, ?)",
				[sender_account, category],
			);
		}
		if (result === null) return;

		return res.send({
			status: "ok"
		});
	});

router.post("/update",
	auth(AuthType.Login),
	required("account_number"), one_required("name", "category"),
	async (req: Request, res: Response) => {
		let {
			account_number, name, category
		} = req.body;

		// Check account validity and user access
		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		// Update the name of the account
		if (name !== undefined) {
			let result = await db.query<ResultSetHeader>(res, "UPDATE folyoszamla SET nev=? WHERE szam=?", [name, account_number]);
			if (result === null) return;
			if (result.affectedRows === 0)
				return res.send({
					status: "error",
					msg: "A megadott folyószámla nem létezik!"
				});
		}

		if (category !== undefined) {
			// First try to update the row with the given account number
			let result = await db.query<ResultSetHeader>(res,
				"UPDATE ceges_folyoszamla SET kategoria_id=? WHERE szam=?",
				[category, account_number]
			);
			if (result === null) return;

			if (result.affectedRows === 0) {
				result = await db.query<ResultSetHeader>(res,
					"INSERT INTO ceges_folyoszamla (szam, kategoria_id) VALUES (?, ?)",
					[account_number, category]);

				if (result === null) return;
			}
		}

		return res.send({
			status: "ok"
		});
	});

router.get(
	"/get",
	auth(AuthType.Login),
	async (req: Request, res: Response) => {
		let accounts = await db.query<RowDataPacket[]>(res,
			`SELECT 
				folyoszamla.szam AS number, 
				nev AS name, 
				egyenleg AS balance, 
				nyitasi_datum AS open_date,
				kategoria_id AS category
			FROM folyoszamla
				LEFT JOIN ceges_folyoszamla ON folyoszamla.szam = ceges_folyoszamla.szam
			WHERE ugyfel_id=?;`,
			[req.session.userid]
		);

		if (!accounts) return;

		return res.send({
			status: "ok",
			accounts
		});
	}
);

router.post(
	"/spending",
	auth(AuthType.Login),
	required("account_number", "start_date", "end_date"),
	async (req: Request, res: Response) => {
		let {
			account_number, start_date, end_date
		} = req.body;

		// Check account validity and user access
		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		let spendings = await db.query<RowDataPacket[]>(res,
			`SELECT 
			SUM(osszeg) AS spending,
			kategoria.nev AS category
			FROM
			tranzakcio
			LEFT JOIN kategoria ON tranzakcio.kategoria_id = kategoria.id
			WHERE tranzakcio.sikeres = 1 AND tranzakcio.kuldo_szamla = ? AND tranzakcio.datum BETWEEN ? AND ?
			GROUP BY kategoria.id;`,
			[account_number, start_date, end_date]
		);
		if (spendings === null) return;

		spendings = spendings.map(s => ({ ...s, spending: Number(s.spending) }));
		return res.send({
			status: "ok",
			spendings
		});
	});

export default router;