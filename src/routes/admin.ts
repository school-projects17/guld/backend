import express, { Request, Response } from 'express';
import { RowDataPacket } from 'mysql2';

import db from "../db/connection";
import { AuthType } from '../middlewares/login';
import auth from '../middlewares/login';

const admin_router = express.Router();

// admin_router.get("/users");
admin_router.get("/accounts",
	auth(AuthType.Admin),
	async (req: Request, res: Response) => {
		let result = await db.query<RowDataPacket[]>(res,
			"SELECT folyoszamla.szam AS szam, folyoszamla.ugyfel_id FROM folyoszamla LEFT JOIN ceges_folyoszamla ON folyoszamla.szam = ceges_folyoszamla.szam"
		);
		if (!result) return;

		return res.send({
			status: "ok",
			accounts: result
		});
	});

admin_router.post("/deleteaddedtransactions",
	auth(AuthType.Admin),
	async (req: Request, res: Response) => {
		if (!await db.query(res, "DELETE FROM tranzakcio WHERE datum > '2022-11-20'")) return;

		return res.send({
			status: "ok"
		});
	});

admin_router.post("/recalculatebalances",
	auth(AuthType.Admin),
	async (req: Request, res: Response) => {
		let accounts = await db.query<RowDataPacket[]>(res,
			"SELECT szam AS number FROM folyoszamla"
		);
		if (!accounts) return;

		for (const { number } of accounts) {
			await db.query<RowDataPacket[]>(res,
				`UPDATE folyoszamla SET egyenleg=
				(SELECT
					SUM(
						CASE
							WHEN tranzakcio.kuldo_szamla = ? THEN -tranzakcio.osszeg
							WHEN tranzakcio.fogado_szamla = ? THEN tranzakcio.osszeg
							ELSE 0
						END
					) AS osszeg
				FROM tranzakcio) WHERE folyoszamla.szam = ?`, [number, number, number]);
		}

		return res.send({
			status: "ok"
		});
	});

export default admin_router;