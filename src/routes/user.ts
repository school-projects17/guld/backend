import express, { Request, Response } from 'express';
import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { SecurePass } from "argon2-pass";

import db from "../db/connection";
import { required } from "../middlewares/required";
import { password_is_valid } from '../utils/verifyPass';
import { random_digits } from "../utils/random";
import { get_current_date } from '../utils/date';

import auth, { AuthType } from '../middlewares/login';

const router = express.Router();

router.get("/data",
	auth(AuthType.Login),
	async (req: Request, res: Response) => {
		let query_result = await db.query<RowDataPacket[]>(res,
			"SELECT vnev AS firstname, knev AS lastname, szul_dat AS birthdate FROM ugyfel WHERE id=?", [req.session.userid]);
		if (query_result === null) return;

		let result = query_result[0];
		return res.send({
			status: "ok",
			result
		});
	});

router.post('/login', required("email", "password"), async (req: Request, res: Response) => {
	let { email, password } = req.body;

	let result = (await db.query<RowDataPacket[]>(res, "SELECT id, jelszo FROM ugyfel WHERE email = ?", [email]));
	if (result === null) return;

	if (result[0] === undefined)
		return res.send({
			status: "error",
			msg: "Nem létezik felhasználó a megadott e-mail címmel!"
		});

	let hash = result[0].jelszo;
	if (!(await password_is_valid(password, hash)))
		return res.send({
			status: "error",
			msg: "Helytelen jelszó!"
		});

	let userid = result[0].id;
	req.session.userid = userid;

	return res.send({
		status: "ok"
	});
});

router.post("/register",
	required("id", "firstname", "lastname", "email", "password", "birthdate"),
	async (req: Request, res: Response) => {
		let {
			id, firstname, lastname, email, password, birthdate
		} = req.body;

		// Hash password
		const sp = new SecurePass();
		const hashed_password = (await sp.hashPassword(Buffer.from(password))).toString("base64");

		// Insert user into the db
		let result = await db.query<ResultSetHeader>(res,
			"INSERT INTO ugyfel (id, vnev, knev, email, jelszo, szul_dat) VALUES (?, ?, ?, ?, ?, ?)",
			[id, firstname, lastname, email, hashed_password, birthdate],
			{ "ER_DUP_ENTRY": "Már létezik ügyfél ezzel az e-mail címmel vagy személyi igazolvány számmal" });
		if (result === null) return;

		// Generate account number for the user
		let account_number = `11773353${random_digits(8)}`;
		result = await db.query<ResultSetHeader>(res,
			"INSERT INTO folyoszamla (szam, egyenleg, nyitasi_datum, ugyfel_id) VALUES (?, ?, ?, ?)",
			[account_number, 0, get_current_date(), id],
		);

		return res.send({
			status: "ok"
		});
	});

router.post("/logout", auth(AuthType.Login), async (req: Request, res: Response) => {
	req.session.destroy((e) => { });

	return res.send({
		status: "ok"
	});
});

export default router;