import express, { Request, Response, Router } from 'express';
import { ResultSetHeader, RowDataPacket } from 'mysql2';

import db from "../db/connection";
import { required, one_required } from "../middlewares/required";
import auth, { AuthType } from '../middlewares/login';
import { random_digits } from "../utils/random";
import { user_owns_account, user_owns_card } from "../utils/query";

const router = Router();

router.post("/create",
	auth(AuthType.Login),
	required("name", "account_number"),
	async (req: Request, res: Response) => {
		let {
			name, account_number,
			take_limit, purchase_limit
		} = req.body;

		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		let card_number = random_digits(16);

		if (!await db.query<RowDataPacket[]>(res,
			`INSERT INTO bankkartya (szam, nev, aktiv, felvetel_limit, vasarlasi_limit, folyoszamla_szam) VALUES (?, ?, ?, ?, ?, ?)`,
			[card_number, name, true, take_limit ?? 150000, purchase_limit ?? null, account_number])) return;

		return res.send({
			status: "ok"
		});
	});

router.post("/update",
	auth(AuthType.Login),
	required("card_number"), one_required("name", "take_limit", "purchase_limit"),
	async (req: Request, res: Response) => {
		let {
			card_number, name,
			take_limit, purchase_limit
		} = req.body;

		// Check account validity and user access
		if (!await user_owns_card(res, req.session.userid, card_number)) return;

		// Update the name of the card
		if (name !== undefined) {
			let result = await db.query<ResultSetHeader>(res, "UPDATE bankkartya SET nev=? WHERE szam=?", [name, card_number]);
			if (result === null) return;
			if (result.affectedRows === 0)
				return res.send({
					status: "error",
					msg: "A megadott bankkártya nem létezik!"
				});
		}

		if (take_limit !== undefined) {
			let result = await db.query<ResultSetHeader>(res,
				"UPDATE bankkartya SET felvetel_limit=? WHERE szam=?",
				[take_limit, card_number]
			);
			if (result === null) return;

			if (result.affectedRows === 0)
				return res.send({
					status: "error",
					msg: "A megadott bankkártya nem lézezik"
				});
		}

		if (purchase_limit !== undefined) {
			let result = await db.query<ResultSetHeader>(res,
				"UPDATE bankkartya SET vasarlasi_limit=? WHERE szam=?",
				[purchase_limit, card_number]
			);
			if (result === null) return;

			if (result.affectedRows === 0)
				return res.send({
					status: "error",
					msg: "A megadott bankkártya nem lézezik"
				});
		}

		return res.send({
			status: "ok"
		});
	});

router.post(
	"/get",
	auth(AuthType.Login),
	required("account_number"),
	async (req: Request, res: Response) => {
		let { account_number } = req.body;

		if (!await user_owns_account(res, req.session.userid, account_number)) return;

		let cards = await db.query<RowDataPacket[]>(res,
			`SELECT 
					szam AS number,
					nev AS name,
					aktiv AS active,
					felvetel_limit AS take_limit,
					vasarlasi_limit AS purchase_limit
				FROM bankkartya 
				WHERE folyoszamla_szam=?`,
			[account_number]
		);
		if (!cards) return;

		return res.send({
			status: "ok",
			cards
		});
	}
);

export default router;