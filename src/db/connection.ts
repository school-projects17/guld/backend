import mysql, { FieldPacket, QueryError } from "mysql2/promise";
import { Response } from "express";

let pool = mysql.createPool({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	database: process.env.DB_NAME,
	password: process.env.DB_PW,
	waitForConnections: true,
	queueLimit: 0
});

async function getConnection() {
	try {
		return await pool.getConnection();
	} catch (err) {
		console.log(`Couldn't establish connection to the database. Reason:\n${err}`);
		process.exit(1);
	}
}

// Try to connect to the database so that if there is an error it doesn't happen on the first request
getConnection().then(conn => conn.release());

export default {
	getConnection,
	query: async <T>(res: Response, query: string, params: Array<any> = [], error_messages: { [key: string]: string; } = {}): Promise<T | null> => {
		let conn = await getConnection();
		try {
			let [results,] = await conn.execute(query, params) as [T, FieldPacket[]];
			return results;
		} catch (e: any) {
			let err = e as QueryError;

			let message = error_messages[err.code];
			if (!message)
				console.log(err);

			res.send({
				status: "error",
				msg: message ?? err
			});
			return null;
		} finally {
			conn.release();
		}
	}
};
