import { SecurePass } from "argon2-pass";

export async function password_is_valid(password: string, hash: string) {
	const sp = new SecurePass();
	const pass_comparison = await sp.verifyHash(Buffer.from(password), Buffer.from(hash, "base64"));

	if (SecurePass.isValid(pass_comparison)) {
		return true;
	} else if (SecurePass.isInvalidOrUnrecognized(pass_comparison) || SecurePass.isInvalid(pass_comparison)) {
		return false;
	} else if (SecurePass.isValidNeedsRehash(pass_comparison)) {
		// Not too secure but ok
		return true;
	}

	return false;
}