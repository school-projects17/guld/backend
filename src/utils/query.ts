import { RowDataPacket } from "mysql2";
import db from "../db/connection";
import { Response } from "express";

export async function user_owns_account(res: Response, userid: string | undefined, account_number: string): Promise<boolean> {
	// Check account validity and user access
	let result = await db.query<RowDataPacket[]>(res,
		"SELECT ugyfel_id AS userid FROM folyoszamla WHERE szam=?", [account_number]);

	if (!result) {
		res.send({
			status: "error",
			msg: "Nem sikerült a számlaszám lekérdezése!"
		});
		return false;
	}

	if (!result[0] || result[0].userid !== userid) {
		console.log(result, userid);
		res.send({
			status: "error",
			msg: "A folyószámla nem létezik vagy nem a te tulajdonodban van!"
		});
		return false;
	}

	return true;
}

export async function user_owns_card(res: Response, userid: string | undefined, card_number: string): Promise<boolean> {
	let result = await db.query<RowDataPacket[]>(res, "SELECT folyoszamla_szam AS account_number FROM bankkartya WHERE szam=?", [card_number]);
	if (!result) {
		res.send({
			status: "error",
			msg: "Nem sikerült a kártyaszám lekérdezése!"
		});
		return false;
	}
	if (!result[0]) {
		res.send({
			status: "error",
			msg: "A megadott kártyaszám nem létezik!"
		});
		return false;
	}

	return await user_owns_account(res, userid, result[0].account_number);
}