import _ from "lodash";

export function random_digits(count: number) {
	return Array.from({ length: count }, () => _.random(9)).join("");
}