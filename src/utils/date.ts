export function get_current_date() {
	let curr_date = new Date();
	return `${curr_date.getFullYear()}-${curr_date.getMonth() + 1}-${curr_date.getDate()}`;
}