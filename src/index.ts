import dotenv from 'dotenv';
dotenv.config();

import express, { Express } from 'express';
import cors from "cors";
import session from "express-session";

import general from "./routes/general";
import account_router from "./routes/account";
import user_router from "./routes/user";
import transaction_router from "./routes/transaction";
import card_router from "./routes/card";
import admin from "./routes/admin";

async function main() {
	const app: Express = express();
	const port = process.env.PORT;

	let required_envs = ["SESSION_SECRET", "ADMIN_KEY", "PORT", "DB_NAME", "DB_HOST", "DB_USER", "DB_PW"];
	let missing_envs = required_envs.filter(env => !(env in process.env));
	if (missing_envs.length > 0) {
		console.log(`Missing environment variables: ${missing_envs}`);
		process.exit(1);
	}

	app.use(cors({
		origin: ["http://localhost:5173", "http://192.168.1.2:5173"],
		credentials: true
	}));
	app.use(express.json());
	app.use(session({
		name: "session",
		secret: process.env.SESSION_SECRET!,
		resave: false,
		saveUninitialized: false,
	}));

	app.use("/user", user_router);
	app.use('/admin', admin);
	app.use('/account', account_router);
	app.use('/transaction', transaction_router);
	app.use('/card', card_router);
	// app.use('/', authed);
	app.use('/', general);

	app.listen(port, () => {
		console.log(`🚀[server]: Server is running at http://localhost:${port}`);
	});
}

main();