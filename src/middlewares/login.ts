import { Request, Response, NextFunction } from "express";

export enum AuthType {
	Login, Admin
}

export default function auth(type: AuthType) {
	return (req: Request, res: Response, next: NextFunction) => {
		if (type === AuthType.Login && req.session.userid === undefined)
			return res.send({
				status: "error",
				msg: "Ahhoz, hogy elérd ezt az oldalt be kell legyél jelentkezve!"
			});

		if (type === AuthType.Admin && req.headers.authorization !== process.env.ADMIN_KEY)
			return res.send({
				status: "error",
				msg: "Helytelen rendszergazdai kulcs!"
			});

		next();
	};
}