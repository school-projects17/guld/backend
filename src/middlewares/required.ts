import { NextFunction, Response, Request } from "express";

export function one_required(...items: string[]) {
	return (req: Request, res: Response, next: NextFunction) => {
		// This only accepts params in the body
		let request_params = req.body;
		// All of the params that are required but not present
		var missing = items.filter(item => !(item in request_params) || request_params[item] === undefined || request_params[item] === "");

		// If all of them are missing then none of them were given
		if (missing.length === items.length)
			return res.status(400).json({
				status: "error",
				msg: `A következő mezők közül legalább az egyik kötelező: ${missing.join(", ")}`
			});

		next();
	};
}

export function required(...items: string[]) {
	return (req: Request, res: Response, next: NextFunction) => {
		// This only accepts params in the body
		let request_params = req.body;
		// All of the params that are required but not present
		var missing = items.filter(item => !(item in request_params) || request_params[item] === undefined || request_params[item] === "");

		if (missing.length > 0)
			return res.status(400).json({
				status: "error",
				msg: `A következő mezők még kötelezőek: ${missing.join(", ")}`
			});

		next();
	};
}
